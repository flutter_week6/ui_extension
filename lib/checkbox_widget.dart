import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value, ValueChanged<bool?>? onChanged) {
  return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(title),
              Expanded(child: Container()),
              Checkbox(value: value, onChanged: onChanged)
            ],
          ),
          Divider()
        ],
      ));
}

class CheckBoxWidget extends StatefulWidget {
  CheckBoxWidget({Key? key}) : super(key: key);

  @override
  _CheckBoxWidgetState createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CheckBox'),
      ),
      body: ListView(
        children: [
          _checkBox('Check1', check1, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('Check2', check2, (value) {
            setState(() {
              check2 = value!;
            });
          }),
          _checkBox('Check3', check3, (value) {
            setState(() {
              check3 = value!;
            });
          }),
          TextButton(
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                        'Check1: $check1, Check2: $check2, Check3: $check3')));
              },
              child: Text('Save'))
        ],
      ),
    );
  }
}
